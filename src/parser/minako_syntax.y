%expect 0

%define api.parser.struct {Parser}
%define api.value.type {Value}
%define api.parser.check_debug { self.debug }

%define parse.error custom
%define parse.trace

%code use {
    // all use goes here
    use crate::{Token, C1Lexer as Lexer, Loc, Value};
}

%code parser_fields {
    errors: Vec<String>,
    /// Enables debug printing
    pub debug: bool,
}

%token
    AND           "&&"
    OR            "||"
    EQ            "=="
    NEQ           "!="
    LEQ           "<="
    GEQ           ">="
    LSS           "<"
    GRT           ">"
    KW_BOOLEAN    "bool"
    KW_DO         "do"
    KW_ELSE       "else"
    KW_FLOAT      "float"
    KW_FOR        "for"
    KW_IF         "if"
    KW_INT        "int"
    KW_PRINTF     "printf"
    KW_RETURN     "return"
    KW_VOID       "void"
    KW_WHILE      "while"
    CONST_INT     "integer literal"
    CONST_FLOAT   "float literal"
    CONST_BOOLEAN "boolean literal"
    CONST_STRING  "string literal"
    ID            "identifier"

// definition of association and precedence of operators
%left '+' '-' OR
%left '*' '/' AND
%nonassoc UMINUS

// workaround for handling dangling else
// LOWER_THAN_ELSE stands for a not existing else
%nonassoc LOWER_THAN_ELSE
%nonassoc KW_ELSE

%%

// ( declassignment ";" | functiondefinition )*
program:
    %empty
    {
        $$ = Value::None;
    }
    | program program_end
    {
        $$ = Value::None;
    }
program_end:
    declassignment ';'
    {
        $$ = Value::None;
    }
    | functiondefinition
    {
        $$ = Value::None;
    }
// type id ( "=" assignment )?
declassignment:
    type id declassignment_opt
    {
        $$ = Value::None;
    }
declassignment_opt:
    %empty
    {
        $$ = Value::None;
    }
    | '=' assignment
    {
        $$ = Value::None;
    }
// <KW_BOOLEAN> | <KW_FLOAT> | <KW_INT> | <KW_VOID>
type:
    KW_BOOLEAN
    {
        $$ = Value::None;
    }
    | KW_FLOAT
    {
        $$ = Value::None;
    }
    | KW_INT
    {
        $$ = Value::None;
    }
    | KW_VOID
    {
        $$ = Value::None;
    }
// <ID>
id:
    ID
    {
        $$ = Value::None;
    }
// id "=" assignment
assignment:
    id '=' assignment
    {
        $$ = Value::None;
    }
    | expr
    {
        $$ = Value::None;
    }
// simpexpr ( "==" simpexpr | "!=" simpexpr | "<=" simpexpr | ">=" simpexpr | "<" simpexpr | ">" simpexpr )?
expr:
    simpexpr expr_opt
    {
        $$ = Value::None;
    }
expr_opt:
    EQ simpexpr
    {
        $$ = Value::None;
    }
    | NEQ simpexpr
    {
      $$ = Value::None;
    }
    | LEQ simpexpr
    {
      $$ = Value::None;
    }
    | GEQ simpexpr
    {
      $$ = Value::None;
    }
    | LSS simpexpr
    {
        $$ = Value::None;
    }
    | GRT simpexpr
    {
        $$ = Value::None;
    }
// ( "-" term | term ) ( "+" term | "-" term | "||" term )*
simpexpr:
    simpexpr_start simpexpr_loop
    {
        $$ = Value::None;
    }
simpexpr_start:
    UMINUS term %prec UMINUS
    {
        $$ = Value::None;
    }
    | term
    {
        $$ = Value::None;
    }
simpexpr_loop:
    %empty
    {
        $$ = Value::None;
    }
    | simpexpr_loop simpexpr_end
    {
        $$ = Value::None;
    }
simpexpr_end:
    '+' term
    {
        $$ = Value::None;
    }
    | '-' term
    {
        $$ = Value::None;
    }
    | OR term
    {
        $$ = Value::None;
    }
// factor ( "*" factor | "/" factor | "&&" factor )*
term:
    factor term_loop
    {
        $$ = Value::None;
    }
term_loop:
    %empty
    {
        $$ = Value::None;
    }
    | term_loop term_end
    {
        $$ = Value::None;
    }
term_end:
    '*' factor
    {
        $$ = Value::None;
    }
    | '/' factor
    {
        $$ = Value::None;
    }
    | AND factor
    {
        $$ = Value::None;
    }
// <CONST_INT> | <CONST_FLOAT> | <CONST_BOOLEAN> | functioncall | id | "(" assignment ")"
factor:
    CONST_INT
    {
        $$ = Value::None;
    }
    | CONST_FLOAT
    {
        $$ = Value::None;
    }
    | CONST_BOOLEAN
    {
        $$ = Value::None;
    }
    | functioncall
    {
        $$ = Value::None;
    }
    | id
    {
        $$ = Value::None;
    }
    | '(' assignment ')'
    {
        $$ = Value::None;
    }
// id "(" ( assignment ( "," assignment )* )? ")"
functioncall:
    id '(' functioncall_opt ')'
    {
        $$ = Value::None;
    }
functioncall_opt:
    %empty
    {
        $$ = Value::None;
    }
    | assignment functioncall_opt_loop
    {
        $$ = Value::None;
    }
functioncall_opt_loop:
    %empty
    {
        $$ = Value::None;
    }
    | functioncall_opt_loop functioncall_opt_end
    {
        $$ = Value::None;
    }
functioncall_opt_end:
    ',' assignment
    {
        $$ = Value::None;
    }
// type id "(" ( parameterlist )? ")" "{" statementlist "}"
functiondefinition:
	type id '(' functiondefinition_opt ')' '{' statementlist '}'
    {
        $$ = Value::None;
    }
functiondefinition_opt:
    %empty
    {
        $$ = Value::None;
    }
    | parameterlist
    {
        $$ = Value::None;
    }
// ( block )*
statementlist:
    %empty
    {
        $$ = Value::None;
    }
    | statementlist block
    {
        $$ = Value::None;
    }
// "{" statementlist "}" | statement
block:
    '{' statementlist '}'
    {
        $$ = Value::None;
    }
    | statement
    {
        $$ = Value::None;
    }
// ifstatement | forstatement | whilestatement | returnstatement ";" | dowhilestatement ";" | printf ";" | declassignment ";" | statassignment ";" | functioncall ";"
statement:
    ifstatement
    {
        $$ = Value::None;
    }
    | forstatement
    {
        $$ = Value::None;
    }
    | whilestatement
    {
        $$ = Value::None;
    }
    | returnstatement ';'
    {
        $$ = Value::None;
    }
    | dowhilestatement ';'
    {
        $$ = Value::None;
    }
    | printf ';'
    {
        $$ = Value::None;
    }
    | declassignment ';'
    {
        $$ = Value::None;
    }
    | statassignment ';'
    {
        $$ = Value::None;
    }
    | functioncall ';'
    {
        $$ = Value::None;
    }
// <KW_IF> "(" assignment ")" statblock ( <KW_ELSE> statblock )?
ifstatement:
    KW_IF '(' assignment ')' statblock %prec LOWER_THAN_ELSE
    {
        $$ = Value::None;
    }
    | KW_IF '(' assignment ')' statblock KW_ELSE statblock %prec KW_ELSE
    {
        $$ = Value::None;
    }
// "{" statementlist "}" | statement
statblock:
    '{' statementlist '}'
    {
        $$ = Value::None;
    }
    | statement
    {
        $$ = Value::None;
    }
// <KW_FOR> "(" ( statassignment | declassignment ) ";" expr ";" statassignment ")" statblock
forstatement:
    KW_FOR '(' forstatement_part ';' expr ';' statassignment ')' statblock
    {
        $$ = Value::None;
    }
forstatement_part:
    statassignment
    {
        $$ = Value::None;
    }
    | declassignment
    {
        $$ = Value::None;
    }
// id "=" assignment
statassignment:
    id '=' assignment
    {
        $$ = Value::None;
    }
// <KW_WHILE> "(" assignment ")" statblock
whilestatement:
    KW_WHILE '(' assignment ')' statblock
    {
        $$ = Value::None;
    }
// <KW_RETURN> ( assignment )?
returnstatement:
    KW_RETURN returnstatement_opt
    {
        $$ = Value::None;
    }
returnstatement_opt:
    %empty
    {
        $$ = Value::None;
    }
    | assignment
    {
        $$ = Value::None;
    }
// <KW_DO> statblock <KW_WHILE> "(" assignment ")"
dowhilestatement:
    KW_DO statblock KW_WHILE '(' assignment ')'
    {
        $$ = Value::None;
    }
// <KW_PRINTF> "(" (assignment | CONST_STRING) ")"
printf:
    KW_PRINTF '(' printf_part ')'
    {
        $$ = Value::None;
    }
printf_part:
    assignment
    {
        $$ = Value::None;
    }
    | CONST_STRING
    {
        $$ = Value::None;
    }
// type id ( "," type id )*
parameterlist:
    type id parameterlist_loop
    {
        $$ = Value::None;
    }
parameterlist_loop:
    %empty
    {
        $$ = Value::None;
    }
    | parameterlist_loop parameterlist_end
    {
        $$ = Value::None;
    }
parameterlist_end:
    ',' type id
    {
        $$ = Value::None;
    }
%%

impl Parser {
    /// "Sucess" status-code of the parser
    pub const ACCEPTED: i32 = -1;

    /// "Failure" status-code of the parser
    pub const ABORTED: i32 = -2;

    /// Constructor
    pub fn new(lexer: Lexer) -> Self {
        // This statement was added to manually remove a dead code warning for 'owned_value_at' which is auto-generated code
        Self::remove_dead_code_warning();
        Self {
            yy_error_verbose: true,
            yynerrs: 0,
            debug: false,
            yyerrstatus_: 0,
            yylexer: lexer,
            errors: Vec::new(),
        }
    }

    /// Wrapper around generated `parse` method that also
    /// extracts the `errors` field and returns it.
    pub fn do_parse(mut self) -> Vec<String> {
        self.parse();
        self.errors
    }

    /// Retrieve the next token from the lexer
    fn next_token(&mut self) -> Token {
        self.yylexer.yylex()
    }

    /// Print a syntax error and add it to the errors field
    fn report_syntax_error(&mut self, stack: &YYStack, yytoken: &SymbolKind, loc: YYLoc) {
        let token_name = yytoken.name();
        let error = format!("Unexpected token {} at {:?}", token_name, loc);
        eprintln!("Stack: {}\nError: {}", stack, error);
        self.errors.push(error);
    }

    /// Helper function that removes a dead code warning, which would otherwise interfere with the correction of a submitted
    /// solution
    fn remove_dead_code_warning() {
    	let mut stack = YYStack::new();
    	let yystate: i32 = 0;
    	let yylval: YYValue = YYValue::new_uninitialized();
    	let yylloc: YYLoc = YYLoc { begin: 0, end: 0 };
        stack.push(yystate, yylval.clone(), yylloc);
    	let _ = stack.owned_value_at(0);
    }
}

